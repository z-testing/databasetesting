﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tSQLt.Database;
using Microsoft.SqlServer.Dac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xunit;

namespace tSQLt.Database.IntegrationTest
{
    [TestClass]
    public class UnitTest1 
    {
        public LocalDatabase  database = new LocalDatabase();

        [Fact]
        [TestMethod]
        public void DeployDacPackageWithoutParameter()
        {
            var packagePath = @"SampleDatabase.dacpac";

            using (var database = new LocalDatabase())
            {
                database.DeployDacpac(packagePath, new DacDeployOptions());
            }
        }

        [Fact]
        [TestMethod]
        public void DeployDacPackageWithParameter()
        {
            var packagePath = @"AdventureWorksExample.dacpac";

            var dacOptions = new DacDeployOptions();
            dacOptions.BlockOnPossibleDataLoss = false;
            dacOptions.CreateNewDatabase = true;

            using (var database = new LocalDatabase())
            {
                database.DeployDacpac(packagePath, dacOptions);
            }
        }

        [TestCleanup]
        public void CleanUp()
        {
            if (database != null)
            {
                database.Dispose();
            }
        }
    }
}
