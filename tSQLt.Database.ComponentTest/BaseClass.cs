﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.SqlServer.Dac;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace tSQLt.Database.ComponentTest
{
    [TestClass]
    public class BaseClass
    {
        private readonly string _connectionString;
        private static string catalogName = "AdventureWorks";

        public BaseClass(string connectionString)
        {
            _connectionString = connectionString;
        }

        protected static string CatalogName
        {
            get { return catalogName; }
        }

        [AssemblyInitialize]
        public static void AssemblyInitialize(TestContext context)
        {
            //InitializeDatabaseViaDacPac();
        }

        [AssemblyCleanup]
        public static void AssemblyCleanup()
        {

        }

        public string Execute(string testName)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tSQLt.Run";
                    cmd.Parameters.AddWithValue("@TestName", testName);
                    string messages = "";

                    connection.FireInfoMessageEventOnUserErrors = true;
                    connection.InfoMessage += (sender, args) =>
                        {
                            messages += "\r\n" + args.Message;
                        };

                    cmd.ExecuteNonQuery();
                    return messages;
                }
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #region Private Method
        private static void InitializeDatabaseViaDacPac()
        {
            // deploy dacpac to database - dacpac is always copied from latest Database project build output
            string dacpacFileName = "AdventureWorks.Database.dacpac";

            // connect to master to initialize database using dacpac services
            SqlConnectionStringBuilder masterConnectionBuilder = new SqlConnectionStringBuilder();

            // TODO Use a temporary instance, and delete it later. Don't use the default instance.
            masterConnectionBuilder.DataSource = "(localdb)\\v11.0";
            masterConnectionBuilder.IntegratedSecurity = true;

            // prepare options
            var dacOptions = new DacDeployOptions();
            dacOptions.BlockOnPossibleDataLoss = false;
            dacOptions.CreateNewDatabase = true;

            using (DacPackage dacpac = DacPackage.Load(dacpacFileName, DacSchemaModelStorageType.Memory))
            {
                // initialize database
                var dacServiceInstance = new DacServices(masterConnectionBuilder.ConnectionString);
                dacServiceInstance.Deploy(dacpac, CatalogName, upgradeExisting: true, options: dacOptions);
            }
        }
        #endregion
    }
}
