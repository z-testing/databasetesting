﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace tSQLt.Database.ComponentTest
{
    [TestClass]
    public class tSQLtTest
    {
        private BaseClass conn;

        [TestInitialize]
        public void Setup()
        {
            conn = new BaseClass("server=.; integrated security=sspi; initial catalog=master"); 
        }

        [TestMethod]
        public void uspUpdateEmployeeHireInfo_currentflag_is_set_to_true_when_hired()
        {
            var messages = conn.Execute("[tSQLt].");

            Assert.IsTrue(messages.Contains("1 succeeded"), "Error running tSQLt test, messages: {0}", messages);
        }

        [TestMethod]
        public void uspUpdateEmployeeHireInfo_currentflag_is_set_to_true_when_hired2()
        {
            var messages = conn.Execute("[HumanResourcesTests].[test uspUpdateEmployeeHireInfo_currentflag_is_set_to_true_when_hired]");

            Assert.IsTrue(messages.Contains("1 succeeded"), "Error running tSQLt test, messages: {0}", messages);
        }
    }
}
